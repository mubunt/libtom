 # *libTom*, a C library to generate a self-extractable archive.

**libTom** is a C library running on LINUX that allows to generate a self-extractable compressed tar archive from a directory. The resulting file appears as a shell script, and can be launched as is. The archive will then uncompress itself via a temporary directory and an optional arbitrary command will be executed (for example an installation script). This is pretty similar to archives generated with "makeself". **libTom** archives also checksums for integrity self-validation (MD5 checksums).

A license must be defined for any application. The text of the license must be accepted by the user during the installation of the application, unless this license is of the "free" type. In this case, it is just displayed for information.

## LICENSE
**libTom** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
### tom_genarchiv
*Synopsys:*
```C
#include "tom.h"
int tom_genarchiv( struct s_archive archiveParam, e_silent silent, int *errorCode );
```
*Description:*
This function generate a self-extractable compressed tar archive from parameters given in the *archiveParam* structure, defined as follows:
```C
struct s_archive {
	char *       archiveName;                        // Application Archive name (result file)
	char *       applicationName;                    // Application name
	char *       applicationRevision;                // Application revision
	char *       applicationDate;                    // Application date
	char *       applicationPath;                    // Application path (origin)
	char *       applicationDescription;             // Short application description text
	char *       applicationLicenseFile;             // License text filename
	unsigned int applicationFreeLicence;             // Free license (1) or not (0)
	char *       applicationPostInstallationScript;  // Post-install script to be executed within the installation directory
};
```
During the generation phase, the different steps and their results are or are not displayed depending on the value of *silent*, respectively **NOSILENT** or **SILENT**.

*Return value:*
The function returns a non-zero status if an error is detected. When it reports an error, *errorCode* has one of the following values:
```C
#define CANNOT_GET_CURRENT_LOCATION             1
#define APPLICATION_DIRECTORY_DOES_NOT_EXIST    2
#define LICENSE_FILE_DOES_NOT_EXIST             3
#define POSTINSTALL_SCRIPT_FILE_DOES_NOT_EXIS   4
#define BASIC_COMMAND_FAILS                     5
```
More details can be delivered by the **tom_latesterror** function.

### tom_version
*Synopsys:*
```C
#include "tom.h"
void tom_version( char *version, size_t length);
```
*Description:*
This function copies the version identification of the **libTom** library, including the terminating null byte ('\0'), to the buffer pointed to by *version*. *length* is the size of this buffer.

### tom_latesterror
*Synopsys:*
```C
#include "tom.h"
char *tom_latesterror( void );
```
*Description:*
This function returns returns an address on the description of the last detected error.

Example of the returned text:
```Bash
ERROR 1: tar command failed ('tar: Blablabla.....')
	Incriminated command: tar -C -czf /home/user/TOM_xmHVAd/myappli.tar.gz bin 2>&1
```

## CODE EXAMPLE
Refer to **[tom](https://gitlab.com/mubunt/tom)**, a command-line utility to generate a self-extractable archive based on **libTom**.

```C
// SYSTEM HEADER FILES
...
// APPLICATION HEADER FILES
#include "tom.h"
#include "tom_cmdline.h"
// CODE
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	struct s_archive archiveParam;
	int errorCode, report;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_tom(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//----  Go on --------------------------------------------------------------
	archiveParam.archiveName = args_info.output_arg;
	archiveParam.applicationName = args_info.name_arg;
	archiveParam.applicationRevision = args_info.revision_arg;
	archiveParam.applicationDate = args_info.date_arg;
	archiveParam.applicationPath = args_info.path_arg;
	archiveParam.applicationDescription = args_info.text_arg;
	archiveParam.applicationLicenseFile = args_info.license_arg;
	archiveParam.applicationPostInstallationScript = args_info.script_arg;
	archiveParam.applicationFreeLicence = args_info.free_given;

	if ((report = tom_genarchiv(archiveParam, NOSILENT, &errorCode)) == EXIT_SUCCESS) {
		fprintf(stdout, "\nSelf extractable archive '%s' successfully generated.\n\n", archiveParam.archiveName);
	} else {
		fprintf(stdout, "\nERROR %d: %s\n\n", errorCode, tom_latesterror());
	}
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_tom_free(&args_info);
	return report;
}
```

## STRUCTURE OF THE APPLICATION
This section walks you through **libTom**'s structure. Once you understand this structure, you will easily find your way around in **libTom**'s code base.

``` bash
$ yaTree
./                        # Application level
├── src/                  # Source directory
│   ├── Makefile          # Makefile
│   ├── tom.h             # Application header file
│   ├── tom_funcs.c       # General functions
│   ├── tom_genarchiv.c   # Main routine
│   ├── tom_internal.h    # Internal header file
│   ├── tom_latesterror.c # 'latesterror' routine
│   ├── tom_script.c      # Script et properties files generator
│   └── tom_version.c     # 'version' routine
├── COPYING.md            # GNU General Public License markdown file
├── LICENSE.md            # License markdown file
├── Makefile              # Makefile
├── README.md             # ReadMe markdown file
├── RELEASENOTES.md       # Release Notes markdown file
└── VERSION               # Version identification text file

1 directories, 14 files
$ 
```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE
```Shell
$ cd libTom
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libTom
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## NOTES
- Based on work by Stephane Peter (makeself-2.2.0) (megastep at megastep.org)

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Developped and tested on XUBUNTU 19.10, GCC v9.2.1.

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***