# RELEASE NOTES: *libTom*, a C library to generate a self-extractable archive.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.0.8**:
  - Updated build system components.

- **Version 1.0.7**:
  - Updated build system.

- **Version 1.0.6**:
  - Removed unused files.

- **Version 1.0.5**:
  - Updated build system component(s)

- **Version 1.0.4**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.0.3**:
  - Some minor changes in .comment file(s).

- **Version 1.0.2**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

**Version 1.0.1**:
  - Updated makefiles and asociated README information.

**Version 1.0.0**:
  - First version.
