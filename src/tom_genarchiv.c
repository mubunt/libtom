//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libTom
// A C library to generate a self-extractable archive
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "tom_internal.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
struct stat			locstat;			// stat variables for EXIST*
struct stat			*ptlocstat;			// stat variables for EXIST*
char 				pending_error[MAX_ERROR];
char 				erroneous_command[MAX_COMMAND];
char				latesterror[MAX_ERROR + MAX_COMMAND + 32];
char				tomLibrary_version[MAX_VERSION];
char				tmppath[PATH_MAX] = "\0";
char				currentpath[PATH_MAX] = "\0";
struct s_tarball	application_tarball		= { "", "", "", "", 0, 0, 0 };
struct s_tarball	postinstall_tarball		= { "", "", "", "", 0, 0, 0 };
struct s_tarball	license_tarball			= { "", "", "", "", 0, 0, 0 };
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int tom_genarchiv( struct s_archive archiveParam, e_silent silent, int *errorCode) {
	int report = EXIT_SUCCESS;
	sighandler_t initial_sigabort;
	sighandler_t initial_sigterm;
	sighandler_t initial_sigint;

	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	pending_error[0] = '\0';

	// Signal processing
	initial_sigabort = signal(SIGABRT, SIG_IGN);
	initial_sigterm = signal(SIGTERM, SIG_IGN);
	initial_sigint = signal(SIGINT, SIG_IGN);

	// Get current location
	if (getcwd(currentpath, sizeof(currentpath)) == NULL) {
		strncpy(pending_error, "Cannot get current location.", MAX_ERROR);
		*errorCode = CANNOT_GET_CURRENT_LOCATION;
		goto ERROR;
	}
	// Get tmp location
	get_tmpdir(currentpath, tmppath);
	// Extract tom application version
	tom_version(tomLibrary_version, sizeof(tomLibrary_version));

	//---- Parameter checking --------------------------------------------------
	if (! EXISTDIR(archiveParam.applicationPath)) {
		snprintf(pending_error, MAX_ERROR, "Application directory '%s' does not exist.", archiveParam.applicationPath);
		*errorCode = APPLICATION_DIRECTORY_DOES_NOT_EXIST;
		goto ERROR;
	}
	if (! EXISTFILE(archiveParam.applicationLicenseFile)) {
		snprintf(pending_error, MAX_ERROR, "License file '%s' does not exist.", archiveParam.applicationLicenseFile);
		*errorCode = LICENSE_FILE_DOES_NOT_EXIST;
		goto ERROR;
	}
	if (archiveParam.applicationPostInstallationScript != NULL &&
	        ! EXISTFILE(archiveParam.applicationPostInstallationScript)) {
		snprintf(pending_error, MAX_ERROR, "Postinstallation script file '%s' does not exist.", archiveParam.applicationPostInstallationScript);
		*errorCode = POSTINSTALL_SCRIPT_FILE_DOES_NOT_EXIST;
		goto ERROR;
	}
	//---- Debug ---------------------------------------------------------------
	//fprintf(stdout, "[libTom] archiveName..........................%s\n", archiveParam.archiveName);
	//fprintf(stdout, "[libTom] applicationName......................%s\n", archiveParam.applicationName);
	//fprintf(stdout, "[libTom] applicationRevision..................%s\n", archiveParam.applicationRevision);
	//fprintf(stdout, "[libTom] applicationDate......................%s\n", archiveParam.applicationDate);
	//fprintf(stdout, "[libTom] applicationPath......................%s\n", archiveParam.applicationPath);
	//fprintf(stdout, "[libTom] applicationDescription...............%s\n", archiveParam.applicationDescription);
	//fprintf(stdout, "[libTom] applicationLicenseFile...............%s\n", archiveParam.applicationLicenseFile);
	//fprintf(stdout, "[libTom] applicationPostInstallationScript....%s\n", archiveParam.applicationPostInstallationScript);
	//fprintf(stdout, "[libTom] applicationFreeLicence...............%d\n", archiveParam.applicationFreeLicence);
	//---- Go on ---------------------------------------------------------------
	// Application archive generation
	if ((report = processTheApplicationTree(archiveParam.applicationPath, silent)) != EXIT_SUCCESS) goto ERROR;
	// License archive generation
	if ((report = processTheLicenseFile(archiveParam.applicationLicenseFile, silent)) != EXIT_SUCCESS) goto ERROR;
	// Startup archive generation
	if (archiveParam.applicationPostInstallationScript != NULL) {
		if ((report = processThePostInstallFile(archiveParam.applicationPostInstallationScript, silent)) != EXIT_SUCCESS) goto ERROR;
	}
	// Self extractable archive generation
	if ((report = processTheSelfExtractableFile(archiveParam, silent)) != EXIT_SUCCESS) goto ERROR;
	//---- Exit ----------------------------------------------------------------
EXIT:
	remove_tmpdir(tmppath);
	signal(SIGABRT, initial_sigabort);
	signal(SIGTERM, initial_sigterm);
	signal(SIGINT, initial_sigint);
	return report;
ERROR:
	report = EXIT_FAILURE;
	goto EXIT;
}
//------------------------------------------------------------------------------
