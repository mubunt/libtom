//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libTom
// A C library to generate a self-extractable archive
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "tom_internal.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static unsigned long long total;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {
	int rv = remove(fpath);
	return rv;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void printONGOING( e_silent silent, const char *action ) {
#define MAX_ONGOING		40
	if (! silent) {
		fprintf(stdout, "%s", action);
		for (size_t i = strlen(action); i < MAX_ONGOING; i++) fprintf(stdout, ".");
		fprintf(stdout, " ");
		fflush(stdout);
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void printSUCCESS( e_silent silent ) {
	if (! silent)
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON "SUCCESS" ESC_ATTRIBUTSOFF "\n", FOREGROUND(green));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void printFAIL( e_silent silent ) {
	if (! silent)
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON "FAILED" ESC_ATTRIBUTSOFF "\n", FOREGROUND(red));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int sum(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {
	total += (unsigned long long) sb->st_size;
	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void prepare_error( int icmd, char *command, char *label ) {
	static const char *cmdname[] = { "dd", "md5sum", "tar"};
	if (icmd == BASE_NONE)
		sprintf(pending_error, "%s", label);
	else
		sprintf(pending_error, "%s command failed ('%s')", cmdname[icmd], label);
	strcpy(erroneous_command, command);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool launch( int base, char *command, char *strresult, unsigned int *uintresult ) {
	FILE *pf;
	char data[512];
	bool result;
	if (strresult != NULL) *strresult = '\0';
	if (uintresult != NULL) *uintresult = 0;

	result = true;
	if (! (pf = popen(command,"r"))) {
		prepare_error(base, command, (char *) "Could not open pipe for output");
		return false;
	}
	if (NULL != fgets(data, 512, pf)) {
		switch (base) {
		case BASE_DD:
			if (strncmp(data, "dd:", 3) == 0) {
				result = false;
				prepare_error(base, command, data);
				while (NULL != fgets(data, 512, pf));
			} else {
				while (NULL != fgets(data, 512, pf)) {
					if (strncmp(data, "dd:", 3) == 0 && ! result) {
						result = false;
						prepare_error(base, command, data);
					}
				}
			}
			break;
		case BASE_MD5SUM:
			if (strncmp(data, "md5sum:", 7) == 0) {
				result = false;
				prepare_error(base, command, data);
				while (NULL != fgets(data, 512, pf));
			} else {
				char *pt = strchr(data, ' ');
				*pt = '\0';
				sprintf(strresult, "%s", data);
			}
			break;
		case BASE_TAR:
			result = false;
			if (data[strlen(data) - 1] == '\n') data[strlen(data) - 1] = '\0';
			prepare_error(base, command, data);
			while (NULL != fgets(data, 512, pf));
			break;
		case BASE_TART:
			++(*uintresult);
			while (NULL != fgets(data, 512, pf)) ++(*uintresult);
			break;
		}
	}
	if (pclose(pf) == -1) {
		if (result) {
			prepare_error(base, command, (char *) "Failed to close command stream");
			result = false;
		}
	}
	return result;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void dirAndBaseName(char *path, char *dir, char *file) {
	*dir = *file = '\0';
	char *pt = path + strlen(path) - 1;
	if (*pt == '/') {
		strcpy(dir, path);
	} else {
		while (*pt != '/' && pt != path) --pt;
		if (*pt == '/') {
			*pt = '\0';
			strcpy(dir, path);
			*pt = '/';
			strcpy(file, (++pt));
		} else {
			strcpy(dir, ".");
			strcpy(file, path);
		}
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static unsigned long long getsizeFile( char *file, char *path ) {
	char absfile[PATH_MAX];
	snprintf(absfile, sizeof(absfile), "%s/%s", path, file);
	return (unsigned long long) SIZE(absfile);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static unsigned long long getsizeTree( char *path ) {
	total = 0;
	nftw(path, sum, 64, FTW_DEPTH | FTW_PHYS);
	return total;
}
//------------------------------------------------------------------------------
static unsigned long long computeNumberBlocks( unsigned long long size ) {
	unsigned long long n = size / DDBLOCKSIZE;
	if (size % DDBLOCKSIZE != 0) n += 1;
	return n;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool execTARcmd( char *tarballname, char *tmpdir, char *datadir, char *file ) {
#define TAR_COMMAND		"tar -C %s -czf %s/%s %s 2>&1"
	char command[MAX_COMMAND];
	snprintf(command, sizeof(command), TAR_COMMAND, datadir, tmpdir, tarballname, file);
	return launch(BASE_TAR, command, NULL, NULL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool execTARTcmd( char *tarballname, char *tmpdir, unsigned int *nbl ) {
#define TAR_COMMAND_T		"tar -tzf %s/%s 2>&1"
	char command[MAX_COMMAND];
	snprintf(command, sizeof(command), TAR_COMMAND_T, tmpdir, tarballname);
	return launch(BASE_TART, command, NULL, nbl);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool execDDcmd( char *output, char *dirout, char *input, char *dirin, unsigned long long size) {
	char command[MAX_COMMAND];
	if (dirout == NULL)
		snprintf(command, sizeof(command), "dd if=%s/%s of=%s ibs=%d count=%llu", dirin, input, output, DDBLOCKSIZE, computeNumberBlocks(size));
	else
		snprintf(command, sizeof(command), "dd if=%s/%s of=%s/%s ibs=%d count=%llu", dirin, input, dirout, output, DDBLOCKSIZE, computeNumberBlocks(size));
	strcat(command, " conv=sync 2>&1");
	return launch(BASE_DD, command, NULL, NULL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool execMD5SUMcmd(char *paddedtarballname, char *tmpdir, char *checksum ) {
#define MD5SUM_COMMAND	"md5sum %s/%s 2>&1"
	char command[MAX_COMMAND];
	snprintf(command, sizeof(command), MD5SUM_COMMAND, tmpdir, paddedtarballname);
	return launch(BASE_MD5SUM, command, checksum, NULL);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void get_tmpdir( char *currendir, char *tmpdir ) {
	static char template[] = "TOM_XXXXXX";
	snprintf(tmpdir, PATH_MAX, "%s/%s", currendir, mkdtemp(template));
}

void remove_tmpdir( char *tmpdir ) {
	if (*tmpdir != '\0')
		nftw(tmpdir, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int processTheLicenseFile( char *license, e_silent silent ) {
	char licensePath[PATH_MAX], licenseFile[PATH_MAX];
	printONGOING(silent, "License archive generation");
	dirAndBaseName(license, licensePath, licenseFile);
	strcpy(license_tarball.localname, licenseFile);
	sprintf(license_tarball.name, "%s%s%s", license_tarball.localname, SUFFIX_TAR, SUFFIX_GZIP);
	snprintf(license_tarball.padded_name, MAX_PADDEDFILENAME, "%s%s", PREFIX_PADDED, license_tarball.name);
	license_tarball.size_once_extracted = getsizeFile(license_tarball.localname, licensePath);
	if (! execTARcmd(license_tarball.name, tmppath, licensePath, license_tarball.localname)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	if (! execDDcmd(license_tarball.padded_name, tmppath, license_tarball.name, tmppath, getsizeFile(license_tarball.name, tmppath))) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	license_tarball.size = getsizeFile(license_tarball.padded_name, tmppath);
	if (! execMD5SUMcmd(license_tarball.padded_name, tmppath, license_tarball.checksum)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	license_tarball.numberoffiles = 1;
	printSUCCESS(silent);
	return EXIT_SUCCESS;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int processTheApplicationTree( char *application, e_silent silent ) {
	char applicationPath[MAX_FILENAME], applicationDir[MAX_FILENAME];
	printONGOING(silent, "Application archive generation");
	dirAndBaseName(application, applicationPath, applicationDir);
	strcpy(application_tarball.localname, applicationDir);
	sprintf(application_tarball.name, "%s%s%s", application_tarball.localname, SUFFIX_TAR, SUFFIX_GZIP);
	snprintf(application_tarball.padded_name, MAX_PADDEDFILENAME, "%s%s", PREFIX_PADDED, application_tarball.name);
	application_tarball.size_once_extracted = getsizeTree(application);
	if (! execTARcmd(application_tarball.name, tmppath,applicationPath, application_tarball.localname)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	if (! execDDcmd(application_tarball.padded_name, tmppath, application_tarball.name, tmppath, getsizeFile(application_tarball.name, tmppath))) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	application_tarball.size = getsizeFile(application_tarball.padded_name, tmppath);
	if (! execMD5SUMcmd(application_tarball.padded_name, tmppath, application_tarball.checksum)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	if (! execTARTcmd(application_tarball.name, tmppath, &application_tarball.numberoffiles)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	printSUCCESS(silent);
	return EXIT_SUCCESS;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int processThePostInstallFile(char *postinstall,  e_silent silent ) {
	char postinstallPath[PATH_MAX], postinstallFile[PATH_MAX];
	printONGOING(silent, "Startup archive generation");
	dirAndBaseName(postinstall, postinstallPath, postinstallFile);
	strcpy(postinstall_tarball.localname, postinstallFile);
	sprintf(postinstall_tarball.name, "%s%s%s", postinstall_tarball.localname, SUFFIX_TAR, SUFFIX_GZIP);
	snprintf(postinstall_tarball.padded_name, MAX_PADDEDFILENAME, "%s%s", PREFIX_PADDED, postinstall_tarball.name);
	postinstall_tarball.size_once_extracted = getsizeFile(postinstall_tarball.localname, postinstallPath);
	if (! execTARcmd(postinstall_tarball.name, tmppath, postinstallPath, postinstall_tarball.localname)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	if (! execDDcmd(postinstall_tarball.padded_name, tmppath, postinstall_tarball.name, tmppath, getsizeFile(postinstall_tarball.name, tmppath))) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	postinstall_tarball.size = getsizeFile(postinstall_tarball.padded_name, tmppath);
	if (! execMD5SUMcmd(postinstall_tarball.padded_name, tmppath, postinstall_tarball.checksum)) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	postinstall_tarball.numberoffiles = 1;
	printSUCCESS(silent);
	return EXIT_SUCCESS;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int processTheSelfExtractableFile( struct s_archive archive, e_silent silent ) {
	char scripttmp[PATH_MAX * 2];
	char paddedscripttmp[PATH_MAX * 2];
	char sskip1[MAX_RELOCATION], sskip2[MAX_RELOCATION], sskip3[MAX_RELOCATION], sskip4[MAX_RELOCATION], sskip5[MAX_RELOCATION];
	printONGOING(silent, "Self extractable archive generation");
	snprintf(scripttmp, sizeof(scripttmp), "%s/%s", tmppath, TMPFILE);
	FILE *fd = fopen(scripttmp, "w");
	if (fd == NULL) {
		printFAIL(silent);
		strncpy(pending_error, "Failed to open temporary file.", MAX_ERROR);
		strcpy(erroneous_command, "");
		return BASIC_COMMAND_FAILS;
	}
	snprintf(sskip1, MAX_RELOCATION, "%020llu", (unsigned long long) 0);
	gen_script(archive, fd, sskip1, sskip1, sskip1);
	fclose(fd);	// close then open instead of "rewind(fd)" to be sure that content is flushed....
	snprintf(paddedscripttmp, sizeof(scripttmp), "%s%s", PREFIX_PADDED, TMPFILE);
	if (! execDDcmd(paddedscripttmp, tmppath, (char *) TMPFILE, tmppath, getsizeFile((char *) TMPFILE, tmppath))) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	unsigned long long skip1 = computeNumberBlocks(getsizeFile(paddedscripttmp, tmppath));
	unsigned long long skip2 = skip1;
	if (archive.applicationPostInstallationScript != NULL) skip2 = skip1 + computeNumberBlocks(getsizeFile(license_tarball.padded_name, tmppath));
	unsigned long long skip3 = skip2 + computeNumberBlocks(getsizeFile(postinstall_tarball.padded_name, tmppath));
	snprintf(sskip1, MAX_RELOCATION, "%020llu", skip1);
	snprintf(sskip2, MAX_RELOCATION, "%020llu", skip2);
	snprintf(sskip3, MAX_RELOCATION, "%020llu", skip3);
	fd = fopen(scripttmp, "w");
	if (fd == NULL) {
		printFAIL(silent);
		strncpy(pending_error, "Failed to re-open temporary file.", MAX_ERROR);
		strcpy(erroneous_command, "");
		return BASIC_COMMAND_FAILS;
	}
	gen_script(archive, fd, sskip1, sskip2, sskip3);
	fclose(fd);
	if (! execDDcmd(archive.archiveName, NULL, (char *) TMPFILE, tmppath, getsizeFile((char *) TMPFILE, tmppath))) {
		printFAIL(silent);
		return BASIC_COMMAND_FAILS;
	}
	fd = fopen(archive.archiveName, "ab");
	if (fd == NULL) {
		printFAIL(silent);
		strncpy(pending_error, "Failed to open result file.", MAX_ERROR);
		strcpy(erroneous_command, "");
		return BASIC_COMMAND_FAILS;
	}
	gen_data(fd, tmppath);
	int n = fchmod(fileno(fd), S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP);
	fclose(fd);
	if (n) {
		printFAIL(silent);
		strncpy(pending_error, "Failed to change permissions of the result file.", MAX_ERROR);
		strcpy(erroneous_command, "");
		return BASIC_COMMAND_FAILS;
	}
	printSUCCESS(silent);
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
