//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libTom
// A C library to generate a self-extractable archive
//------------------------------------------------------------------------------
#ifndef TOM_INTERNAL_H
#define TOM_INTERNAL_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
//#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdarg.h>
#include <ftw.h>
#include <fcntl.h>
//#include <termcap.h>
//#include <termios.h>
#include <dirent.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "tom.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define APPLICATIONFILE		"tom.application"
#define TMPFILE				"tom.tmp"

#define SUFFIXSHELL			".sh"
#define SUFFIX_TAR			".tar"
#define SUFFIX_GZIP			".gz"
#define PREFIX_PADDED		"Padded_"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define BASE_NONE			-1
#define BASE_DD				0
#define BASE_MD5SUM			1
#define BASE_TAR			2
#define BASE_TART			3
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define MAX_VERSION			128
#define MAX_ERROR			2048
#define MAX_FILENAME		512
#define MAX_PADDEDFILENAME	544
#define MAX_CHECKSUM		33
#define MAX_COMMAND			2048
#define MAX_RELOCATION		32
#define DDBLOCKSIZE			4096
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define ESC_ATTRIBUTSOFF	"\033[0m"
#define ESC_STRONG_ON		"\033[1m"
#define ESC_COLOR			"\033[%dm"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define FOREGROUND(color) 	(30 + color)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define EXIST(x)			(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)			(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
#define EXISTFILE(y)		(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFREG)
#define SIZE(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_size)
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { black = 0, red, green, yellow, blue, magenta, cyan, white } e_color;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct s_tarball {
	char					name[MAX_FILENAME + 16];
	char					localname[MAX_FILENAME];
	char					padded_name[MAX_PADDEDFILENAME];
	char					checksum[MAX_CHECKSUM];
	unsigned long long		size;
	unsigned long long		size_once_extracted;
	unsigned int 			numberoffiles;
} s_tarball;
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern char					tomLibrary_version[];
extern char 				pending_error[];
extern char 				erroneous_command[];
extern char					latesterror[];
extern char					tmppath[];
extern char					currentpath[];
extern struct s_tarball 	application_tarball;
extern struct s_tarball 	postinstall_tarball;
extern struct s_tarball 	license_tarball;
extern struct stat			locstat;
extern struct stat			*ptlocstat;
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
// tom_funcs.c
extern void	get_tmpdir( char *, char * );
extern void	remove_tmpdir( char * );
extern int 	processTheLicenseFile( char *, e_silent );
extern int 	processTheApplicationTree( char *, e_silent );
extern int 	processThePostInstallFile( char *, e_silent );
extern int 	processTheSelfExtractableFile( struct s_archive, e_silent );
// tom_script.c
extern void gen_script( struct s_archive, FILE *, char *, char *, char * );
extern void gen_data( FILE *, char * );
//------------------------------------------------------------------------------
#endif	// TOM_INTERNAL_H
