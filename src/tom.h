//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libTom
// A C library to generate a self-extractable archive
//------------------------------------------------------------------------------
#ifndef TOM_H
#define TOM_H
//------------------------------------------------------------------------------
// ERROR CODES
//------------------------------------------------------------------------------
#define CANNOT_GET_CURRENT_LOCATION				1
#define APPLICATION_DIRECTORY_DOES_NOT_EXIST	2
#define LICENSE_FILE_DOES_NOT_EXIST				3
#define POSTINSTALL_SCRIPT_FILE_DOES_NOT_EXIST	4
#define BASIC_COMMAND_FAILS						5
//------------------------------------------------------------------------------
// ENUMS
//------------------------------------------------------------------------------
typedef enum { NOSILENT = 0, SILENT } e_silent;
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
typedef struct s_archive {
	char 			*archiveName;						// Application Archive name (result file)
	char 			*applicationName;					// Application name
	char 			*applicationRevision;				// Application revision
	char 			*applicationDate;					// Application date
	char 			*applicationPath;					// Application path (origin)
	char 			*applicationDescription;				// Short application description text
	char 			*applicationLicenseFile;				// License text filename
	unsigned int 	applicationFreeLicence;				// Free license (1) or not (0)
	char 			*applicationPostInstallationScript;	// Post-install script to be executed within the installation directory
} s_archive;
//------------------------------------------------------------------------------
// ROUTINES DEFINITIONS
//------------------------------------------------------------------------------
extern int 		tom_genarchiv( struct s_archive, e_silent, int * );
extern void		tom_version( char *, size_t );
extern char 	*tom_latesterror( void );
//------------------------------------------------------------------------------
#endif	// TOM_H
