//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libTom
// A C library to generate a self-extractable archive
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "tom_internal.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void bincat( FILE *fd, char *path, char *binfile ) {
	char absfile[PATH_MAX];
	char buffer[DDBLOCKSIZE];

	snprintf(absfile, sizeof(absfile), "%s/%s", path, binfile);
	FILE *ptr = fopen(absfile, "rb");

	while ( (fread(buffer, sizeof(buffer), 1, ptr)) == 1)
		fwrite(buffer, sizeof(buffer), 1, fd);

	fclose(ptr);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void gen_script( struct s_archive archive, FILE *fd, char *skip1, char *skip2, char *skip3 ) {
	unsigned long long neededdiskspace = (license_tarball.size_once_extracted
	                                      + postinstall_tarball.size_once_extracted
	                                      + application_tarball.size_once_extracted) / 1024 + 1;
	fprintf(fd, "#!/bin/bash\n");
	fprintf(fd, "##############################################################\n");
	fprintf(fd, "# This script was generated using Tom version %s\n", tomLibrary_version);
	fprintf(fd, "##############################################################\n");
	fprintf(fd, "umask 077\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Initializations\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "TOM_VERSION=this_version\n");
	fprintf(fd, "VERBOSE=yes\n");
	fprintf(fd, "CLEAN=yes\n");
	fprintf(fd, "YES=\n");
	fprintf(fd, "TAR_CMD=tar\n");
	fprintf(fd, "MD5_CMD=md5sum\n");
	fprintf(fd, "DD_CMD=dd\n");
	fprintf(fd, "DF_CMD=df\n");
	fprintf(fd, "ECHO_CMD=echo\n");
	fprintf(fd, "ECHO_ESC=\"echo -e\"\n");
	fprintf(fd, "TPUT_CMD=tput\n");
	fprintf(fd, "NUMBERofCOLS=\"$( ${TPUT_CMD} cols )\"\n");
	fprintf(fd, "NUMBERofLINES=\"$( ${TPUT_CMD} lines )\"\n");
	fprintf(fd, "FMT_CMD=\"fmt -u -w ${NUMBERofCOLS}\"\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "INSTALLKIT=${0}\n");
	fprintf(fd, "HERE=$( pwd )\n");
	fprintf(fd, "TMPROOT=${TMPDIR:=/tmp}\n");
	fprintf(fd, "TMP=${TMPROOT}/tom.${RANDOM}\n");
	fprintf(fd, "BLOCKSIZE=%d\n", DDBLOCKSIZE);
	fprintf(fd, "DISK_AVAILABLE_KB=\n");
	fprintf(fd, "HLIGHTTURNON=$(tput bold)\n");
	fprintf(fd, "NORMAL=$(tput sgr0)\n");
	fprintf(fd, "YELLOW=$(tput setaf 3)\n");
	fprintf(fd, "HEADERLINE=\"	\"\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "APPLICATION_NAME=\"%s\"\n", archive.applicationName);
	fprintf(fd, "APPLICATION_REVISION=\"%s\"\n", archive.applicationRevision);
	fprintf(fd, "APPLICATION_DATE=\"%s\"\n", archive.applicationDate);
	fprintf(fd, "APPLICATION_DESCRIPTION=\"%s\"\n", archive.applicationDescription);
	fprintf(fd, "APPLICATION_NEEDED_DSKSPACE=%llu\n", neededdiskspace);
	fprintf(fd, "APPLICATION_TARBALL=%s\n", application_tarball.name);
	fprintf(fd, "APPLICATION_CHECKSUM=%s\n", application_tarball.checksum);
	fprintf(fd, "APPLICATION_NBFILES=%d\n", application_tarball.numberoffiles);
	fprintf(fd, "APPLICATION_STARTUP=%s\n", postinstall_tarball.localname);
	fprintf(fd, "APPLICATION_STARTUPCHECKSUM=%s\n", postinstall_tarball.checksum);
	fprintf(fd, "APPLICATION_LICENSE=%s\n", license_tarball.localname);
	fprintf(fd, "APPLICATION_LICENSECHECKSUM=%s\n", license_tarball.checksum);
	fprintf(fd, "APPLICATION_INSTALLATION_PATH=\n");
	fprintf(fd, "FREE=%d\n", archive.applicationFreeLicence);
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Ctrl-C trap\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "trap 'byebye -1' 1 2 3 4 6 8 10 12 13 15\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Progress bar\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function progressBar_init {\n");
	fprintf(fd, "	local i\n");
	fprintf(fd, "	_PB_ECHO_CMD=\"echo -n\"\n");
	fprintf(fd, "	_PB_ECHO_ESC=\"echo -e\"\n");
	fprintf(fd, "	_PB_TPUT_CMD=tput\n");
	fprintf(fd, "	_PB_COLS=\"$( ${_PB_TPUT_CMD} cols )\"\n");
	fprintf(fd, "	_PB_CURSOR_HIDE=\"$( ${_PB_TPUT_CMD} civis )\"\n");
	fprintf(fd, "	_PB_CURSOR_VISIBLE=\"$( ${_PB_TPUT_CMD} cnorm )\"\n");
	fprintf(fd, "	_PB_CURSOR_READ=\"\\033[6n\"\n");
	fprintf(fd, "	_PB_LIGHTSHADE='░'\n");
	fprintf(fd, "	_PB_BLOCK='█'\n");
	fprintf(fd, "	_PB_REVERSE=\"$( ${_PB_TPUT_CMD} rev)\"\n");
	fprintf(fd, "	_PB_NORMAL=\"$( ${_PB_TPUT_CMD} sgr0)\"\n");
	fprintf(fd, "	_PB_MAXVALUE=${1}\n");
	fprintf(fd, "	_PB_NBCOLS=$( expr ${_PB_COLS} - 6 )\n");
	fprintf(fd, "	${_PB_ECHO_ESC} -n ${_PB_CURSOR_READ}\n");
	fprintf(fd, "	read -sdR _PB_CURPOS\n");
	fprintf(fd, "	_PB_CURPOS=$( echo ${_PB_CURPOS#*\\[} | sed -e \"s/;.*//\")\n");
	fprintf(fd, "	((_PB_CURPOS--))\n");
	fprintf(fd, "	printf \"%%s%%3d%%%% \" ${_PB_CURSOR_HIDE} 0\n");
	fprintf(fd, "	for i in $( seq 0 1 ${_PB_NBCOLS}); do\n");
	fprintf(fd, "		${_PB_ECHO_CMD} ${_PB_LIGHTSHADE}\n");
	fprintf(fd, "	done\n");
	fprintf(fd, "}\n");
	fprintf(fd, "function progressBar_close {\n");
	fprintf(fd, "	${_PB_ECHO_ESC} ${_PB_CURSOR_VISIBLE}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "function progressBar_display {\n");
	fprintf(fd, "	local percent\n");
	fprintf(fd, "	local n\n");
	fprintf(fd, "	local value=${1}\n");
	fprintf(fd, "	if [ ${value} -lt 0 ]; then value=0; fi\n");
	fprintf(fd, "	percent=$( expr 100 \\* ${value} / ${_PB_MAXVALUE} )\n");
	fprintf(fd, "	n=$( expr ${_PB_NBCOLS} \\* ${percent} / 100 )\n");
	fprintf(fd, "	n=${n%%.*}\n");
	fprintf(fd, "	n=$( expr ${n} - ${#value} - 2)\n");
	fprintf(fd, "	printf \"%%s%%3d%%%% \" $( ${_PB_TPUT_CMD} cup ${_PB_CURPOS} 0 ) ${percent}\n");
	fprintf(fd, "	for i in $( seq 0 1 ${n} ); do\n");
	fprintf(fd, "		${_PB_ECHO_CMD} ${_PB_BLOCK}\n");
	fprintf(fd, "	done\n");
	fprintf(fd, "	${_PB_ECHO_CMD} ${_PB_REVERSE} ${value} ${_PB_NORMAL}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Internal Functions\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function Error {\n");
	fprintf(fd, "	${ECHO_CMD} \"!!! ERROR !!! $1. Abort ...\"\n");
	fprintf(fd, "	byebye 1\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function Exist { \n");
	fprintf(fd, "	if [ ! -f $1 ]; then Error \"Wrong installation. File $1 does not exist\"; fi\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function printheaderline {\n");
	fprintf(fd, "	local n\n");
	fprintf(fd, "	${ECHO_CMD}\n");
	fprintf(fd, "	${ECHO_CMD} -n `seq 1 4 | sed 's/.*/━/' | tr -d '\n'`\n");
	fprintf(fd, "	${ECHO_CMD} -n \" ${1} \"\n");
	fprintf(fd, "	n=`expr ${NUMBERofCOLS} - ${#1} - 6`\n");
	fprintf(fd, "	${ECHO_CMD} `seq 1 ${n} | sed 's/.*/━/' | tr -d '\n'`\n");
	fprintf(fd, "	${ECHO_CMD}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function printline {\n");
	fprintf(fd, "	${ECHO_CMD} `seq 1 ${NUMBERofCOLS} | sed 's/.*/━/' | tr -d '\n'`\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function printstep {\n");
	fprintf(fd, "	${ECHO_ESC} ${YELLOW}${1}${NORMAL}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function testenvironment {\n");
	fprintf(fd, "	local s\n");
	fprintf(fd, "	[ \"x$DISPLAY\" = \"x\" ] && Error \"DISPLAY not initialized\"\n");
	fprintf(fd, "	s=`exec <&- 2>&-; which ${MD5_CMD} || type ${MD5_CMD}`\n");
	fprintf(fd, "	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${MD5_CMD}' does not exist\"\n");
	fprintf(fd, "	s=`exec <&- 2>&-; which ${TAR_CMD} || type ${TAR_CMD}`\n");
	fprintf(fd, "	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${TAR_CMD}' does not exist\"\n");
	fprintf(fd, "	s=`exec <&- 2>&-; which ${DD_CMD} || type ${DD_CMD}`\n");
	fprintf(fd, "	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${DD_CMD}' does not exist\"\n");
	fprintf(fd, "	s=`exec <&- 2>&-; which ${DF_CMD} || type ${DF_CMD}`\n");
	fprintf(fd, "	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${DF_CMD}' does not exist\"\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function getcompressedfiles {\n");
	fprintf(fd, "	local remainder\n");
	fprintf(fd, "	ACTUALSIZE=${2}\n");
	fprintf(fd, "	SKIP=${3}\n");
	fprintf(fd, "	BLOCKS=`expr ${ACTUALSIZE} / ${BLOCKSIZE}`\n");
	fprintf(fd, "	remainder=`expr ${ACTUALSIZE} %c ${BLOCKSIZE}`\n", '%');
	fprintf(fd, "	if [ ${remainder:-0} -gt 0 ]; then BLOCKS=`expr $BLOCKS + 1`; fi\n");
	fprintf(fd, "	${DD_CMD} if=${INSTALLKIT} of=${1} bs=${BLOCKSIZE} count=${BLOCKS} skip=${SKIP} 2>/dev/null\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function checkchecksum {\n");
	fprintf(fd, "	local checksum\n");
	fprintf(fd, "	checksum=$( ${MD5_CMD} ${1} | cut -b-32 )\n");
	fprintf(fd, "	[ \"${2}\" == \"${checksum}\" ] || Error \"${3}: Wrong checksum\"\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function checkdiskspace {\n");
	fprintf(fd, "	DISK_AVAILABLE_KB=$( ${DF_CMD} -kP ${TMPROOT} | tail -1 | awk '{ print $4 }' )\n");
	fprintf(fd, "	[ ${DISK_AVAILABLE_KB} -gt ${APPLICATION_NEEDED_DSKSPACE} ] || Error \"Not enough space left in '${TMPROOT}'' (${DISK_AVAILABLE_KB}KB). Needed ${APPLICATION_NEEDED_DSKSPACE}KB\"\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function untar {\n");
	fprintf(fd, "	local untar\n");
	fprintf(fd, "	cd ${TMP}\n");
	fprintf(fd, "	${TAR_CMD} zxf ${1} 2>/dev/null\n");
	fprintf(fd, "	untar=$?\n");
	fprintf(fd, "	cd ${HERE}\n");
	fprintf(fd, "	[ $untar -ne 0 ] && Error \"Untar failed\"\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function questionYorN {\n");
	fprintf(fd, "	local yn\n");
	fprintf(fd, "	while true; do\n");
	fprintf(fd, "		${ECHO_ESC} -n \"${HEADERLINE}${HLIGHTTURNON}${1} [Yes|No] ${NORMAL}\"\n");
	fprintf(fd, "		read yn\n");
	fprintf(fd, "		case $yn in\n");
	fprintf(fd, "			[Yy]*)	return 0;;\n");
	fprintf(fd, "			[Nn]*)	return 1;;\n");
	fprintf(fd, "			*)		${ECHO_CMD} \"${HEADERLINE}Please answer 'yes' or 'no'.\";;\n");
	fprintf(fd, "		esac\n");
	fprintf(fd, "	done\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function getlicenseagreement {\n");
	fprintf(fd, "	cd ${TMP}\n");
	fprintf(fd, "	printheaderline \"License agreement\"\n");
	fprintf(fd, "	${FMT_CMD} ${APPLICATION_LICENSE}\n");
	fprintf(fd, "	printline\n");
	fprintf(fd, "	cd ${HERE}\n");
	fprintf(fd, "	if [ ! -z ${YES} ] || [ ${FREE} -eq 1 ]; then return 0; fi\n");
	fprintf(fd, "	if questionYorN \"Do you agree with the terms of that license?\"; then return 0; fi\n");
	fprintf(fd, "	return 1\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function summary {\n");
	fprintf(fd, "	printheaderline \"Installation summary\"\n");
	fprintf(fd, "	${ECHO_ESC} \"${HEADERLINE}${HLIGHTTURNON}Product Name:${NORMAL}\"\n");
	fprintf(fd, "	${ECHO_CMD} \"${HEADERLINE}	${APPLICATION_NAME} Revision ${APPLICATION_REVISION} - ${APPLICATION_DATE}\"\n");
	fprintf(fd, "	${ECHO_CMD} \"${HEADERLINE}	${APPLICATION_DESCRIPTION}\"\n");
	fprintf(fd, "	${ECHO_ESC} \"${HEADERLINE}${HLIGHTTURNON}Installation Directory:${NORMAL}\"\n");
	fprintf(fd, "	${ECHO_CMD} \"${HEADERLINE}	${APPLICATION_INSTALLATION_PATH}\"\n");
	fprintf(fd, "	${ECHO_ESC} \"${HEADERLINE}${HLIGHTTURNON}Disk Space Information:${NORMAL}\"\n");
	fprintf(fd, "	${ECHO_CMD} \"${HEADERLINE}	Required:  ${APPLICATION_NEEDED_DSKSPACE} KB\"\n");
	fprintf(fd, "	${ECHO_CMD} \"${HEADERLINE}	Available: ${DISK_AVAILABLE_KB} KB\"\n");
	fprintf(fd, "	${ECHO_ESC} \"${HEADERLINE}${HLIGHTTURNON}Post-install Script:${NORMAL}\"\n");
	fprintf(fd, "	if [ -z ${APPLICATION_STARTUP} ]; then ${ECHO_CMD} \"${HEADERLINE}	None\"; else ${ECHO_CMD} \"${HEADERLINE}	Yes\"; fi\n");
	fprintf(fd, "	printline\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function getinstallationpath {\n");
	fprintf(fd, "	local answer\n");
	fprintf(fd, "	APPLICATION_INSTALLATION_PATH=\"\"\n");
	fprintf(fd, "	${ECHO_CMD}\n");
	fprintf(fd, "	while true; do\n");
	fprintf(fd, "		${ECHO_ESC} -n \"${HEADERLINE}${HLIGHTTURNON}Installation directory? ${NORMAL}\"\n");
	fprintf(fd, "		read answer\n");
	fprintf(fd, "		if [ -z ${answer} ]; then\n");
	fprintf(fd, "			if questionYorN \"Need an answer! Do you want to stop?\"; then return 1; fi\n");
	fprintf(fd, "		else\n");
	fprintf(fd, "			answer=$( echo ${answer} | sed s!~!${HOME}/!g )\n");
	fprintf(fd, "			answer=$( realpath ${answer} )\n");
	fprintf(fd, "			if [ ! -d ${answer} ]; then\n");
	fprintf(fd, "				if questionYorN \"Nonexistent directory! Do you want to stop?\"; then return 1; fi\n");
	fprintf(fd, "			else\n");
	fprintf(fd, "				break\n");
	fprintf(fd, "			fi\n");
	fprintf(fd, "		fi\n");
	fprintf(fd, "	done\n");
	fprintf(fd, "	APPLICATION_INSTALLATION_PATH=${answer}\n");
	fprintf(fd, "	return 0\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function installation {\n");
	fprintf(fd, "	local i\n");
	fprintf(fd, "	local n\n");
	fprintf(fd, "	cd ${TMP}\n");
	fprintf(fd, "	progressBar_init ${APPLICATION_NBFILES}\n");
	fprintf(fd, "	n=0\n");
	fprintf(fd, "	while read i; do\n");
	fprintf(fd, "		(( n++ ))\n");
	fprintf(fd, "		if [ $((${n}%%5)) -eq 0 ]; then\n");
	fprintf(fd, "			progressBar_display ${n}\n");
	fprintf(fd, "		fi\n");
	fprintf(fd, "	done < <(${TAR_CMD} -C ${APPLICATION_INSTALLATION_PATH} -vxzf ${APPLICATION_TARBALL})\n");
	fprintf(fd, "	progressBar_display ${n}\n");
	fprintf(fd, "	progressBar_close\n");
	fprintf(fd, "	cd ${HERE}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function postinstall {\n");
	fprintf(fd, "	chmod +x ${TMP}/${APPLICATION_STARTUP}\n");
	fprintf(fd, "	cd ${APPLICATION_INSTALLATION_PATH}\n");
	fprintf(fd, "	printheaderline \"Post-install script\"\n");
	fprintf(fd, "	${TMP}/${APPLICATION_STARTUP}\n");
	fprintf(fd, "	printline\n");
	fprintf(fd, "	cd ${HERE}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "function byebye {\n");
	fprintf(fd, "	[[ ${1} -eq -1 ]] && ${ECHO_CMD} \"CTRL-C trapped.\"\n");
	fprintf(fd, "	[ ${VERBOSE} ] && ${ECHO_CMD} \"Exiting...\"\n");
	fprintf(fd, "	[ ${CLEAN} ] && rm -fr ${TMP}\n");
	fprintf(fd, "	exit ${1}\n");
	fprintf(fd, "}\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Main\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Argument parsing\n");
	fprintf(fd, "internalOPTIONS=\n");
	fprintf(fd, "while [[ $# -gt 0 ]]; do\n");
	fprintf(fd, "	key=\"$1\"\n");
	fprintf(fd, "	case $key in\n");
	fprintf(fd, "		-h|--help)\n");
	fprintf(fd, "			${ECHO_CMD} \"Self-extractable archive ${APPLICATION_NAME} Revision ${APPLICATION_REVISION} - ${APPLICATION_DATE}\"\n");
	fprintf(fd, "			${ECHO_CMD} \"${APPLICATION_DESCRIPTION}\"\n");
	fprintf(fd, "			${ECHO_CMD}\n");
	fprintf(fd, "			${ECHO_CMD} \"Usage: ${INSTALLKIT} [OPTION] ...\"\n");
	fprintf(fd, "			${ECHO_CMD}\n");
	fprintf(fd, "			${ECHO_CMD} \"-h, --help         Print this help and exit\"\n");
	fprintf(fd, "			${ECHO_CMD} \"-s, --silent       Silent mode\"\n");
	fprintf(fd, "			${ECHO_CMD} \"-y, --yes          Implicitely agree the license\"\n");
	fprintf(fd, "			${ECHO_CMD} \"-p, --path PATH    Installation path\"\n");
	fprintf(fd, "			${ECHO_CMD}\n");
	fprintf(fd, "			byebye 0\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "		-s|--silence)\n");
	fprintf(fd, "			VERBOSE=\n");
	fprintf(fd, "			shift # past value\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "		-y|--yes)\n");
	fprintf(fd, "			YES=1\n");
	fprintf(fd, "			shift # past value\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "		-p|--path)\n");
	fprintf(fd, "			APPLICATION_INSTALLATION_PATH=\"$2\"\n");
	fprintf(fd, "			if [ ! -d ${APPLICATION_INSTALLATION_PATH} ]; then\n");
	fprintf(fd, "				Error \"Nonexistent directory $2\"\n");
	fprintf(fd, "			fi\n");
	fprintf(fd, "			shift # past argument\n");
	fprintf(fd, "			shift # past value\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "	    -*|--*)\n");
	fprintf(fd, "			Error \"Unknown option $1\"\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "		*)\n");
	fprintf(fd, "			Error \"Unknown parameter $1\"\n");
	fprintf(fd, "			;;\n");
	fprintf(fd, "	esac\n");
	fprintf(fd, "done\n");
	fprintf(fd, "#### Test system requirements\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Ckecking system requirements...\"\n");
	fprintf(fd, "testenvironment\n");
	fprintf(fd, "#### Temporary directory creation\n");
	fprintf(fd, "mkdir -p ${TMP}\n");
	fprintf(fd, "#### Test if needed disk space available\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Checking available disk space...\"\n");
	fprintf(fd, "checkdiskspace\n");
	fprintf(fd, "#### Get installation resources\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Extracting the installation resources...\"\n");
	fprintf(fd, "getcompressedfiles ${TMP}/${APPLICATION_LICENSE} %llu %s\n", license_tarball.size, skip1);
	fprintf(fd, "[ ${APPLICATION_STARTUP} ] && getcompressedfiles ${TMP}/${APPLICATION_STARTUP} %llu %s\n", postinstall_tarball.size, skip2);
	fprintf(fd, "getcompressedfiles ${TMP}/${APPLICATION_TARBALL} %llu %s\n", application_tarball.size, skip3);
	fprintf(fd, "#### Verifying archive integrity...\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Verifying installation resources integrity...\"\n");
	fprintf(fd, "checkchecksum ${TMP}/${APPLICATION_LICENSE} ${APPLICATION_LICENSECHECKSUM} \"License file\"\n");
	fprintf(fd, "[ ${APPLICATION_STARTUP} ] && checkchecksum ${TMP}/${APPLICATION_STARTUP} ${APPLICATION_STARTUPCHECKSUM} \"Startup file\"\n");
	fprintf(fd, "checkchecksum ${TMP}/${APPLICATION_TARBALL} ${APPLICATION_CHECKSUM} \"Application file\"\n");
	fprintf(fd, "#### Untar installation resources, except application to be installed\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Unpacking installation resources...\"\n");
	fprintf(fd, "untar ${APPLICATION_LICENSE}\n");
	fprintf(fd, "[ ${APPLICATION_STARTUP} ] && untar ${APPLICATION_STARTUP}\n");
	fprintf(fd, "#### License agreement\n");
	fprintf(fd, "if ! getlicenseagreement; then byebye 1; fi\n");
	fprintf(fd, "#### Installation path\n");
	fprintf(fd, "if [ -z ${APPLICATION_INSTALLATION_PATH} ]; then\n");
	fprintf(fd, "	if ! getinstallationpath; then byebye 1; fi\n");
	fprintf(fd, "fi\n");
	fprintf(fd, "#### Installation summary\n");
	fprintf(fd, "summary\n");
	fprintf(fd, "#### Installation\n");
	fprintf(fd, "[ ${VERBOSE} ] && printstep \"Installing\"\n");
	fprintf(fd, "installation\n");
	fprintf(fd, "#### Post-install script\n");
	fprintf(fd, "[ ${APPLICATION_STARTUP} ] && postinstall\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "byebye 0\n");
	fprintf(fd, "####----------------------------------------------------------\n");
	fprintf(fd, "#### Installation Resources\n");
	fprintf(fd, "####----------------------------------------------------------\n");
}
//------------------------------------------------------------------------------
void gen_data( FILE *fd, char *path ) {
	bincat(fd, path, license_tarball.padded_name);
	bincat(fd, path, postinstall_tarball.padded_name);
	bincat(fd, path, application_tarball.padded_name);
}
//------------------------------------------------------------------------------
